import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreelayoutComponent } from './treelayout.component';

describe('TreelayoutComponent', () => {
  let component: TreelayoutComponent;
  let fixture: ComponentFixture<TreelayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreelayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreelayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
