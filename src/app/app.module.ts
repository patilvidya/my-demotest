import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ANALYZE_FOR_ENTRY_COMPONENTS } from '@angular/core';


import { AppComponent } from './app.component';
import { TreelayoutComponent } from './treelayout/treelayout.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import {
  MatIconModule,
  MatNativeDateModule,
  MatTreeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
} from '@angular/material';
import { RouterModule, ROUTES, Routes, } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

export const routes: Routes = [
 
 
];
@NgModule({
  declarations: [
    AppComponent,
    TreelayoutComponent,
    
  ],
  imports: [
    BrowserModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatTreeModule,
    MatIconModule,
    FormsModule,
    RouterModule.forRoot([]),
    MatNativeDateModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
  ],

    
    entryComponents: [TreelayoutComponent],
  
    bootstrap: [TreelayoutComponent],
})
export class AppModule { }

export function getRoutes() {
  return routes;
}

